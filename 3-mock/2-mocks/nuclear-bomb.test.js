import { sendBombSignal, dieTogether } from './nuclear-bomb';

test('请测试 - sendBombSignal 会向 bomb 函数传递 O_o 作为起爆指令', () => {
  const mockDieTogether = jest.fn().mockImplementation(signal => signal === 'O_o');
  sendBombSignal(mockDieTogether);
  expect(mockDieTogether).toHaveBeenCalledWith('O_o');
});
